BYFN_CA1_PRIVATE_KEY=$(cd crypto-config/peerOrganizations/cepa21.baw-network.com/ca && ls *_sk)
BYFN_CA2_PRIVATE_KEY=$(cd crypto-config/peerOrganizations/crdo.baw-network.com/ca && ls *_sk)

echo "$(
sed -e "s/\${BYFN_CA1_PRIVATE_KEY_VALUE}/$BYFN_CA1_PRIVATE_KEY/" \
-e "s/\${BYFN_CA2_PRIVATE_KEY_VALUE}/$BYFN_CA2_PRIVATE_KEY/" \
-e "s/\${P1PORT}/$P1PORT/" \
./.env.template \
)" > .env
