# Copyright IBM Corp. All Rights Reserved.
#
# SPDX-License-Identifier: Apache-2.0
#

version: '3'

volumes:
  orderer.baw-network.com:
  peer0.cepa21.baw-network.com:
  peer1.cepa21.baw-network.com:
  peer0.crdo.baw-network.com:
  peer1.crdo.baw-network.com:
  pgdata:
  walletstore:

networks:
  stage_byfn:
    external: true
    driver: overlay

services:
  orderer-baw-network-com:
    deploy:
      replicas: 1
      placement:
        constraints:
          - node.hostname == ${ORDERER}
    image: hyperledger/fabric-orderer:1.4
    volumes:
      - /home/ec2-user/network/baw-network/channel-artifacts/genesis.block:/var/hyperledger/orderer/orderer.genesis.block
      - /home/ec2-user/network/baw-network/crypto-config/ordererOrganizations/baw-network.com/orderers/orderer.baw-network.com/msp:/var/hyperledger/orderer/msp
      - /home/ec2-user/network/baw-network/crypto-config/ordererOrganizations/baw-network.com/orderers/orderer.baw-network.com/tls/:/var/hyperledger/orderer/tls
      - orderer.baw-network.com:/var/hyperledger/production/orderer
    environment:
      - FABRIC_LOGGING_SPEC=INFO
      - ORDERER_GENERAL_LISTENADDRESS=0.0.0.0
      - ORDERER_GENERAL_GENESISMETHOD=file
      - ORDERER_GENERAL_GENESISFILE=/var/hyperledger/orderer/orderer.genesis.block
      - ORDERER_GENERAL_LOCALMSPID=OrdererMSP
      - ORDERER_GENERAL_LOCALMSPDIR=/var/hyperledger/orderer/msp
      - ORDERER_GENERAL_TLS_ENABLED=true
      - ORDERER_GENERAL_TLS_PRIVATEKEY=/var/hyperledger/orderer/tls/server.key
      - ORDERER_GENERAL_TLS_CERTIFICATE=/var/hyperledger/orderer/tls/server.crt
      - ORDERER_GENERAL_TLS_ROOTCAS=[/var/hyperledger/orderer/tls/ca.crt]
      - ORDERER_KAFKA_TOPIC_REPLICATIONFACTOR=1
      - ORDERER_KAFKA_VERBOSE=true
      - ORDERER_GENERAL_CLUSTER_CLIENTCERTIFICATE=/var/hyperledger/orderer/tls/server.crt
      - ORDERER_GENERAL_CLUSTER_CLIENTPRIVATEKEY=/var/hyperledger/orderer/tls/server.key
      - ORDERER_GENERAL_CLUSTER_ROOTCAS=[/var/hyperledger/orderer/tls/ca.crt]
    working_dir: /opt/gopath/src/github.com/hyperledger/fabric
    command: orderer
    ports:
      - 7050:7050
    networks:
      stage_byfn:
        aliases:
          - orderer.baw-network.com

  mysql:
    image: mysql:5.7.19
    deploy:
      replicas: 1
      placement:
        constraints:
          - node.hostname == ${ORDERER}
    ports:
      - 3306:3306
    environment:
      - MYSQL_DATABASE=baw_database
      - MYSQL_USER=dev
      - MYSQL_PASSWORD=devpass
      - MYSQL_ROOT_PASSWORD=rootpass
    networks:
      stage_byfn:
        aliases:
          - mysql.container

  adminer:
    image: adminer
    deploy:
      replicas: 1
      placement:
        constraints:
          - node.hostname == ${ORDERER}
    ports:
      - 8080:8080
    #volumes:
    #  - postgres
    networks:
      stage_byfn:
        aliases:
          - adminer.container

  peer0-cepa21-baw-network-com:
    deploy:
      replicas: 1
      placement:
        constraints:
          - node.hostname == ${CEPA21}
    image: hyperledger/fabric-peer:1.4
    environment:
      - CORE_PEER_ADDRESSAUTODETECT=true
      - CORE_VM_DOCKER_ATTACHSTDOUT=true
      - CORE_VM_ENDPOINT=unix:///host/var/run/docker.sock
      - CORE_VM_DOCKER_HOSTCONFIG_NETWORKMODE=stage_byfn
      - FABRIC_LOGGING_SPEC=INFO
      - CORE_PEER_TLS_ENABLED=true
      - CORE_PEER_GOSSIP_USELEADERELECTION=true
      - CORE_PEER_GOSSIP_ORGLEADER=false
      - CORE_PEER_PROFILE_ENABLED=true
      - CORE_PEER_TLS_CERT_FILE=/etc/hyperledger/fabric/tls/server.crt
      - CORE_PEER_TLS_KEY_FILE=/etc/hyperledger/fabric/tls/server.key
      - CORE_PEER_TLS_ROOTCERT_FILE=/etc/hyperledger/fabric/tls/ca.crt
      - CORE_PEER_ID=peer0.cepa21.baw-network.com
      - CORE_PEER_ADDRESS=peer0.cepa21.baw-network.com:7051
      - CORE_PEER_LISTENADDRESS=0.0.0.0:7051
      - CORE_PEER_CHAINCODEADDRESS=peer0.cepa21.baw-network.com:7052
      - CORE_PEER_CHAINCODELISTENADDRESS=0.0.0.0:7052
      - CORE_PEER_GOSSIP_BOOTSTRAP=peer1.cepa21.baw-network.com:8051
      - CORE_PEER_GOSSIP_EXTERNALENDPOINT=peer0.cepa21.baw-network.com:7051
      - CORE_PEER_LOCALMSPID=Cepa21MSP
      - CORE_LEDGER_STATE_STATEDATABASE=CouchDB
      - CORE_LEDGER_STATE_COUCHDBCONFIG_COUCHDBADDRESS=couchdb0:5984
      - CORE_LEDGER_STATE_COUCHDBCONFIG_USERNAME=
      - CORE_LEDGER_STATE_COUCHDBCONFIG_PASSWORD=

      - CORE_CHAINCODE_LOGGING_SHIM=debug
      - CORE_CHAINCODE_LOGGING_LEVEL=debug
    working_dir: /opt/gopath/src/github.com/hyperledger/fabric/peer
    command: peer node start
    volumes:
      - /var/run/:/host/var/run/
      - /home/ec2-user/network/baw-network/crypto-config/peerOrganizations/cepa21.baw-network.com/peers/peer0.cepa21.baw-network.com/msp:/etc/hyperledger/fabric/msp
      - /home/ec2-user/network/baw-network/crypto-config/peerOrganizations/cepa21.baw-network.com/peers/peer0.cepa21.baw-network.com/tls:/etc/hyperledger/fabric/tls
      - peer0.cepa21.baw-network.com:/var/hyperledger/production
    ports:
      - 7051:7051
    networks:
      stage_byfn:
        aliases:
          - peer0.cepa21.baw-network.com
    depends_on:
      - couchdb0

  peer1-cepa21-baw-network-com:
    deploy:
      replicas: 1
      placement:
        constraints:
          - node.hostname == ${CEPA21}
    environment:
      - CORE_PEER_ADDRESSAUTODETECT=true
      - CORE_VM_DOCKER_ATTACHSTDOUT=true
      - CORE_PEER_ID=peer1.cepa21.baw-network.com
      - CORE_PEER_ADDRESS=peer1.cepa21.baw-network.com:8051
      - CORE_PEER_LISTENADDRESS=0.0.0.0:8051
      - CORE_PEER_CHAINCODEADDRESS=peer1.cepa21.baw-network.com:8052
      - CORE_PEER_CHAINCODELISTENADDRESS=0.0.0.0:8052
      - CORE_PEER_GOSSIP_EXTERNALENDPOINT=peer1.cepa21.baw-network.com:8051
      - CORE_PEER_GOSSIP_BOOTSTRAP=peer0.cepa21.baw-network.com:7051
      - CORE_PEER_LOCALMSPID=Cepa21MSP
      - CORE_VM_ENDPOINT=unix:///host/var/run/docker.sock
      - CORE_VM_DOCKER_HOSTCONFIG_NETWORKMODE=stage_byfn
      - FABRIC_LOGGING_SPEC=INFO
      - CORE_PEER_TLS_ENABLED=true
      - CORE_PEER_GOSSIP_USELEADERELECTION=true
      - CORE_PEER_GOSSIP_ORGLEADER=false
      - CORE_PEER_PROFILE_ENABLED=true
      - CORE_PEER_TLS_CERT_FILE=/etc/hyperledger/fabric/tls/server.crt
      - CORE_PEER_TLS_KEY_FILE=/etc/hyperledger/fabric/tls/server.key
      - CORE_PEER_TLS_ROOTCERT_FILE=/etc/hyperledger/fabric/tls/ca.crt
      - CORE_LEDGER_STATE_STATEDATABASE=CouchDB
      - CORE_LEDGER_STATE_COUCHDBCONFIG_COUCHDBADDRESS=couchdb1:5984
      - CORE_LEDGER_STATE_COUCHDBCONFIG_USERNAME=
      - CORE_LEDGER_STATE_COUCHDBCONFIG_PASSWORD=

      - CORE_CHAINCODE_LOGGING_SHIM=debug
      - CORE_CHAINCODE_LOGGING_LEVEL=debug
    volumes:
      - /var/run/:/host/var/run/
      - /home/ec2-user/network/baw-network/crypto-config/peerOrganizations/cepa21.baw-network.com/peers/peer1.cepa21.baw-network.com/msp:/etc/hyperledger/fabric/msp
      - /home/ec2-user/network/baw-network/crypto-config/peerOrganizations/cepa21.baw-network.com/peers/peer1.cepa21.baw-network.com/tls:/etc/hyperledger/fabric/tls
      - peer1.cepa21.baw-network.com:/var/hyperledger/production
    image: hyperledger/fabric-peer:1.4
    working_dir: /opt/gopath/src/github.com/hyperledger/fabric/peer
    command: peer node start
    ports:
      - 8051:8051
    networks:
      stage_byfn:
        aliases:
          - peer1.cepa21.baw-network.com
    depends_on:
      - couchdb1

  peer0-crdo-baw-network-com:
    deploy:
      replicas: 1
      placement:
        constraints:
          - node.hostname == ${CRDO}
    environment:
      - CORE_PEER_ADDRESSAUTODETECT=true
      - CORE_VM_DOCKER_ATTACHSTDOUT=true
      - CORE_PEER_ID=peer0.crdo.baw-network.com
      - CORE_PEER_ADDRESS=peer0.crdo.baw-network.com:9051
      - CORE_PEER_LISTENADDRESS=0.0.0.0:9051
      - CORE_PEER_CHAINCODEADDRESS=peer0.crdo.baw-network.com:9052
      - CORE_PEER_CHAINCODELISTENADDRESS=0.0.0.0:9052
      - CORE_PEER_GOSSIP_EXTERNALENDPOINT=peer0.crdo.baw-network.com:9051
      - CORE_PEER_GOSSIP_BOOTSTRAP=peer1.crdo.baw-network.com:10051
      - CORE_PEER_LOCALMSPID=CrdoMSP
      - CORE_VM_ENDPOINT=unix:///host/var/run/docker.sock
      - CORE_VM_DOCKER_HOSTCONFIG_NETWORKMODE=stage_byfn
      - FABRIC_LOGGING_SPEC=INFO
      - CORE_PEER_TLS_ENABLED=true
      - CORE_PEER_GOSSIP_USELEADERELECTION=true
      - CORE_PEER_GOSSIP_ORGLEADER=false
      - CORE_PEER_PROFILE_ENABLED=true
      - CORE_PEER_TLS_CERT_FILE=/etc/hyperledger/fabric/tls/server.crt
      - CORE_PEER_TLS_KEY_FILE=/etc/hyperledger/fabric/tls/server.key
      - CORE_PEER_TLS_ROOTCERT_FILE=/etc/hyperledger/fabric/tls/ca.crt
      - CORE_LEDGER_STATE_STATEDATABASE=CouchDB
      - CORE_LEDGER_STATE_COUCHDBCONFIG_COUCHDBADDRESS=couchdb2:5984
      - CORE_LEDGER_STATE_COUCHDBCONFIG_USERNAME=
      - CORE_LEDGER_STATE_COUCHDBCONFIG_PASSWORD=
      - CORE_CHAINCODE_LOGGING_SHIM=debug
      - CORE_CHAINCODE_LOGGING_LEVEL=debug
    volumes:
      - /var/run/:/host/var/run/
      - /home/ec2-user/network/baw-network/crypto-config/peerOrganizations/crdo.baw-network.com/peers/peer0.crdo.baw-network.com/msp:/etc/hyperledger/fabric/msp
      - /home/ec2-user/network/baw-network/crypto-config/peerOrganizations/crdo.baw-network.com/peers/peer0.crdo.baw-network.com/tls:/etc/hyperledger/fabric/tls
      - peer0.crdo.baw-network.com:/var/hyperledger/production
    image: hyperledger/fabric-peer:1.4
    working_dir: /opt/gopath/src/github.com/hyperledger/fabric/peer
    command: peer node start
    ports:
      - 9051:9051
    networks:
      stage_byfn:
        aliases:
          - peer0.crdo.baw-network.com
    depends_on:
      - couchdb2

  peer1-crdo-baw-network-com:
    deploy:
      replicas: 1
      placement:
        constraints:
          - node.hostname == ${CRDO}
    environment:
      - CORE_PEER_ADDRESSAUTODETECT=true
      - CORE_VM_DOCKER_ATTACHSTDOUT=true
      - CORE_PEER_ID=peer1.crdo.baw-network.com
      - CORE_PEER_ADDRESS=peer1.crdo.baw-network.com:10051
      - CORE_PEER_LISTENADDRESS=0.0.0.0:10051
      - CORE_PEER_CHAINCODEADDRESS=peer1.crdo.baw-network.com:10052
      - CORE_PEER_CHAINCODELISTENADDRESS=0.0.0.0:10052
      - CORE_PEER_GOSSIP_EXTERNALENDPOINT=peer1.crdo.baw-network.com:10051
      - CORE_PEER_GOSSIP_BOOTSTRAP=peer0.crdo.baw-network.com:9051
      - CORE_PEER_LOCALMSPID=CrdoMSP
      - CORE_VM_ENDPOINT=unix:///host/var/run/docker.sock
      - CORE_VM_DOCKER_HOSTCONFIG_NETWORKMODE=stage_byfn
      - FABRIC_LOGGING_SPEC=INFO
      - CORE_PEER_TLS_ENABLED=true
      - CORE_PEER_GOSSIP_USELEADERELECTION=true
      - CORE_PEER_GOSSIP_ORGLEADER=false
      - CORE_PEER_PROFILE_ENABLED=true
      - CORE_PEER_TLS_CERT_FILE=/etc/hyperledger/fabric/tls/server.crt
      - CORE_PEER_TLS_KEY_FILE=/etc/hyperledger/fabric/tls/server.key
      - CORE_PEER_TLS_ROOTCERT_FILE=/etc/hyperledger/fabric/tls/ca.crt
      - CORE_LEDGER_STATE_STATEDATABASE=CouchDB
      - CORE_LEDGER_STATE_COUCHDBCONFIG_COUCHDBADDRESS=couchdb3:5984
      - CORE_LEDGER_STATE_COUCHDBCONFIG_USERNAME=
      - CORE_LEDGER_STATE_COUCHDBCONFIG_PASSWORD=

      - CORE_CHAINCODE_LOGGING_SHIM=debug
      - CORE_CHAINCODE_LOGGING_LEVEL=debug
    volumes:
      - /var/run/:/host/var/run/
      - /home/ec2-user/network/baw-network/crypto-config/peerOrganizations/crdo.baw-network.com/peers/peer1.crdo.baw-network.com/msp:/etc/hyperledger/fabric/msp
      - /home/ec2-user/network/baw-network/crypto-config/peerOrganizations/crdo.baw-network.com/peers/peer1.crdo.baw-network.com/tls:/etc/hyperledger/fabric/tls
      - peer1.crdo.baw-network.com:/var/hyperledger/production
    ports:
      - 10051:10051
    image: hyperledger/fabric-peer:1.4
    working_dir: /opt/gopath/src/github.com/hyperledger/fabric/peer
    command: peer node start
    networks:
      stage_byfn:
        aliases:
          - peer1.crdo.baw-network.com
    depends_on:
      - couchdb3

  cli:
    deploy:
      replicas: 1
      placement:
        constraints:
          - node.hostname == ${CEPA21}
    image: hyperledger/fabric-tools:1.4
    tty: true
    stdin_open: true
    environment:
      - SYS_CHANNEL=$SYS_CHANNEL
      - GOPATH=/opt/gopath
      - CORE_VM_ENDPOINT=unix:///host/var/run/docker.sock
      - FABRIC_LOGGING_SPEC=INFO
      - CORE_PEER_ID=cli
      - CORE_PEER_ADDRESS=peer0.cepa21.baw-network.com:7051
      - CORE_PEER_LOCALMSPID=Cepa21MSP
      - CORE_PEER_TLS_ENABLED=true
      - CORE_PEER_TLS_CERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/cepa21.baw-network.com/peers/peer0.cepa21.baw-network.com/tls/server.crt
      - CORE_PEER_TLS_KEY_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/cepa21.baw-network.com/peers/peer0.cepa21.baw-network.com/tls/server.key
      - CORE_PEER_TLS_ROOTCERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/cepa21.baw-network.com/peers/peer0.cepa21.baw-network.com/tls/ca.crt
      - CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/cepa21.baw-network.com/users/Admin@cepa21.baw-network.com/msp
    working_dir: /opt/gopath/src/github.com/hyperledger/fabric/peer
    command: /bin/bash
    volumes:
        - /var/run/:/host/var/run/
        - /home/ec2-user/network/chaincode/:/opt/gopath/src/github.com/chaincode
        - /home/ec2-user/network/baw-network/crypto-config:/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/
        - /home/ec2-user/network/baw-network/scripts:/opt/gopath/src/github.com/hyperledger/fabric/peer/scripts/
        - /home/ec2-user/network/baw-network/channel-artifacts:/opt/gopath/src/github.com/hyperledger/fabric/peer/channel-artifacts
    depends_on:
      - orderer.baw-network.com
      - peer0.cepa21.baw-network.com
      - peer1.cepa21.baw-network.com
      - peer0.crdo.baw-network.com
      - peer1.crdo.baw-network.com
    networks:
      stage_byfn:
        aliases:
          - cli

  ca0:
    deploy:
      replicas: 1
      placement:
        constraints:
          - node.hostname == ${CEPA21}
    image: hyperledger/fabric-ca:1.4
    environment:
      - FABRIC_CA_HOME=/etc/hyperledger/fabric-ca-server
      - FABRIC_CA_SERVER_CA_NAME=ca-cepa21
      - FABRIC_CA_SERVER_TLS_ENABLED=true
      - FABRIC_CA_SERVER_TLS_CERTFILE=/etc/hyperledger/fabric-ca-server-config/ca.cepa21.baw-network.com-cert.pem
      - FABRIC_CA_SERVER_TLS_KEYFILE=/etc/hyperledger/fabric-ca-server-config/${BYFN_CA1_PRIVATE_KEY}
      - FABRIC_CA_SERVER_PORT=7054
    ports:
      - "7054:7054"
    command: sh -c 'fabric-ca-server start --ca.certfile /etc/hyperledger/fabric-ca-server-config/ca.cepa21.baw-network.com-cert.pem --ca.keyfile /etc/hyperledger/fabric-ca-server-config/${BYFN_CA1_PRIVATE_KEY} -b admin:adminpw -d'
    volumes:
      - /home/ec2-user/network/baw-network/crypto-config/peerOrganizations/cepa21.baw-network.com/ca/:/etc/hyperledger/fabric-ca-server-config
    networks:
      stage_byfn:
        aliases:
          - ca.cepa21.baw-network.com

  ca1:
    deploy:
      replicas: 1
      placement:
        constraints:
          - node.hostname == ${CRDO}
    image: hyperledger/fabric-ca:1.4
    environment:
      - FABRIC_CA_HOME=/etc/hyperledger/fabric-ca-server
      - FABRIC_CA_SERVER_CA_NAME=ca-crdo
      - FABRIC_CA_SERVER_TLS_ENABLED=true
      - FABRIC_CA_SERVER_TLS_CERTFILE=/etc/hyperledger/fabric-ca-server-config/ca.crdo.baw-network.com-cert.pem
      - FABRIC_CA_SERVER_TLS_KEYFILE=/etc/hyperledger/fabric-ca-server-config/${BYFN_CA2_PRIVATE_KEY}
      - FABRIC_CA_SERVER_PORT=8054
    ports:
      - "8054:8054"
    command: sh -c 'fabric-ca-server start --ca.certfile /etc/hyperledger/fabric-ca-server-config/ca.crdo.baw-network.com-cert.pem --ca.keyfile /etc/hyperledger/fabric-ca-server-config/${BYFN_CA2_PRIVATE_KEY} -b admin:adminpw -d'
    volumes:
      - /home/ec2-user/network/baw-network/crypto-config/peerOrganizations/crdo.baw-network.com/ca/:/etc/hyperledger/fabric-ca-server-config
    networks:
      stage_byfn:
        aliases:
          - ca.crdo.baw-network.com

  couchdb0:
    deploy:
      replicas: 1
      placement:
        constraints:
          - node.hostname == ${CEPA21}
    image: hyperledger/fabric-couchdb
    environment:
      - COUCHDB_USER=
      - COUCHDB_PASSWORD=
    ports:
      - "5984:5984"
    networks:
      stage_byfn:
        aliases:
          - couchdb0

  couchdb1:
    deploy:
      replicas: 1
      placement:
        constraints:
          - node.hostname == ${CEPA21}
    image: hyperledger/fabric-couchdb
    environment:
      - COUCHDB_USER=
      - COUCHDB_PASSWORD=
    ports:
      - "6984:5984"
    networks:
      stage_byfn:
        aliases:
          - couchdb1

  couchdb2:
    deploy:
      replicas: 1
      placement:
        constraints:
          - node.hostname == ${CRDO}
    image: hyperledger/fabric-couchdb
    environment:
      - COUCHDB_USER=
      - COUCHDB_PASSWORD=
    ports:
      - "7984:5984"
    networks:
      stage_byfn:
        aliases:
          - couchdb2

  couchdb3:
    deploy:
      replicas: 1
      placement:
        constraints:
          - node.hostname == ${CRDO}
    image: hyperledger/fabric-couchdb
    environment:
      - COUCHDB_USER=
      - COUCHDB_PASSWORD=
    ports:
      - "8984:5984"
    networks:
      stage_byfn:
        aliases:
          - couchdb3

  explorerdb-baw-network-com:
    deploy:
      replicas: 1
      placement:
        constraints:
          - node.hostname == ${CEPA21}
    image: hyperledger/explorer-db:1.1.2
    hostname: explorerdb.baw-network.com
    environment:
      - DATABASE_DATABASE=fabricexplorer
      - DATABASE_USERNAME=hppoc
      - DATABASE_PASSWORD=password
    healthcheck:
      test: "pg_isready -h localhost -p 5432 -q -U postgres"
      interval: 30s
      timeout: 10s
      retries: 5
    volumes:
      - pgdata:/var/lib/postgresql/data
    networks:
      stage_byfn:
        aliases:
          - explorerdb.baw-network.com
  explorer-baw-network-com:
    deploy:
      replicas: 1
      placement:
        constraints:
          - node.hostname == ${CEPA21}
    image: hyperledger/explorer:1.1.2
    hostname: explorer.baw-network.com
    environment:
      - DATABASE_HOST=explorerdb.baw-network.com
      - DATABASE_DATABASE=fabricexplorer
      - DATABASE_USERNAME=hppoc
      - DATABASE_PASSWD=password
      - LOG_LEVEL_APP=debug
      - LOG_LEVEL_DB=debug
      - LOG_LEVEL_CONSOLE=info
      - LOG_CONSOLE_STDOUT=true
      - DISCOVERY_AS_LOCALHOST=false
    volumes:
      - /home/ec2-user/network/baw-network/explorer/config.json:/opt/explorer/app/platform/fabric/config.json
      - /home/ec2-user/network/baw-network/explorer/connection-profile:/opt/explorer/app/platform/fabric/connection-profile
      - /home/ec2-user/network/baw-network/crypto-config:/tmp/crypto
      - walletstore:/opt/wallet
    command: sh -c "node /opt/explorer/main.js && tail -f /dev/null"
    depends_on:
      - explorerdb-baw-network-com
    ports:
      - "8090:8080"
    networks:
      stage_byfn:
        aliases:
          - explorer.baw-network.com
