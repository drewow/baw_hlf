DELAY="3"
LANGUAGE="golang"

CORE_PEER_LOCALMSPID="Cepa21MSP"
CORE_PEER_TLS_ROOTCERT_FILE=$PEER0_CEPA21_CA
CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/cepa21.baw-network.com/users/Admin@cepa21.baw-network.com/msp
CORE_PEER_ADDRESS=peer0.cepa21.baw-network.com:7051
peer channel create -o orderer.baw-network.com:7050 -c cepa21crdo -f ./channel-artifacts/cepa21crdo.tx --tls true --cafile /opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/baw-network.com/orderers/orderer.baw-network.com/msp/tlscacerts/tlsca.baw-network.com-cert.pem

ORDERER_CA=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/baw-network.com/orderers/orderer.baw-network.com/msp/tlscacerts/tlsca.baw-network.com-cert.pem
PEER0_CEPA21_CA=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/cepa21.baw-network.com/peers/peer0.cepa21.baw-network.com/tls/ca.crt
PEER0_CRDO_CA=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/crdo.baw-network.com/peers/peer0.crdo.baw-network.com/tls/ca.crt

# JOIN PEERS ORGANISATION'S TO cepa21crdo CHANNEL

# JOIN PEERS Cepa21MSP TO cepa21crdo CHANNEL
CORE_PEER_LOCALMSPID="Cepa21MSP"
CORE_PEER_TLS_ROOTCERT_FILE=$PEER0_CEPA21_CA
CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/cepa21.baw-network.com/users/Admin@cepa21.baw-network.com/msp

CORE_PEER_ADDRESS=peer0.cepa21.baw-network.com:7051
peer channel join -b cepa21crdo.block

CORE_PEER_ADDRESS=peer1.cepa21.baw-network.com:8051
peer channel join -b cepa21crdo.block

# JOIN PEERS CrdoMSP TO cepa21crdo CHANNEL
CORE_PEER_LOCALMSPID="CrdoMSP"
CORE_PEER_TLS_ROOTCERT_FILE=$PEER0_CRDO_CA
CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/crdo.baw-network.com/users/Admin@crdo.baw-network.com/msp

CORE_PEER_ADDRESS=peer0.crdo.baw-network.com:9051
peer channel join -b cepa21crdo.block

CORE_PEER_ADDRESS=peer1.crdo.baw-network.com:10051
peer channel join -b cepa21crdo.block

# UPDATE PEERS IN cepa21crdo CHANNEL

CORE_PEER_LOCALMSPID="Cepa21MSP"
CORE_PEER_TLS_ROOTCERT_FILE=$PEER0_CEPA21_CA
CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/cepa21.baw-network.com/users/Admin@cepa21.baw-network.com/msp
CORE_PEER_ADDRESS=peer0.cepa21.baw-network.com:7051
peer channel update -o orderer.baw-network.com:7050 -c cepa21crdo -f ./channel-artifacts/Cepa21MSPanchors_cepa21crdo.tx --tls true --cafile /opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/baw-network.com/orderers/orderer.baw-network.com/msp/tlscacerts/tlsca.baw-network.com-cert.pem

CORE_PEER_LOCALMSPID="CrdoMSP"
CORE_PEER_TLS_ROOTCERT_FILE=$PEER0_CRDO_CA
CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/crdo.baw-network.com/users/Admin@crdo.baw-network.com/msp
CORE_PEER_ADDRESS=peer0.crdo.baw-network.com:9051
peer channel update -o orderer.baw-network.com:7050 -c cepa21crdo -f ./channel-artifacts/CrdoMSPanchors_cepa21crdo.tx --tls true --cafile /opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/baw-network.com/orderers/orderer.baw-network.com/msp/tlscacerts/tlsca.baw-network.com-cert.pem

# INSTALLING SMART CONTRACT ON ORGANISATION'S PEERS
CC_RUNTIME_LANGUAGE=golang
CC_SRC_PATH=github.com/chaincode/baw-contract/
CONFIG_ROOT=/opt/gopath/src/github.com/hyperledger/fabric/peer
CEPA21_MSPCONFIGPATH=${CONFIG_ROOT}/crypto/peerOrganizations/cepa21.baw-network.com/users/Admin@cepa21.baw-network.com/msp
CEPA21_TLS_ROOTCERT_FILE=${CONFIG_ROOT}/crypto/peerOrganizations/cepa21.baw-network.com/peers/peer0.cepa21.baw-network.com/tls/ca.crt
CRDO_MSPCONFIGPATH=${CONFIG_ROOT}/crypto/peerOrganizations/crdo.baw-network.com/users/Admin@crdo.baw-network.com/msp
CRDO_TLS_ROOTCERT_FILE=${CONFIG_ROOT}/crypto/peerOrganizations/crdo.baw-network.com/peers/peer0.crdo.baw-network.com/tls/ca.crt
ORDERER_TLS_ROOTCERT_FILE=${CONFIG_ROOT}/crypto/ordererOrganizations/baw-network.com/orderers/orderer.baw-network.com/msp/tlscacerts/tlsca.baw-network.com-cert.pem

# BAW CHAINCODE
# Installing smart contract on peer0.cepa21.baw-network.com
CORE_PEER_LOCALMSPID=Cepa21MSP
CORE_PEER_ADDRESS=peer0.cepa21.baw-network.com:7051
CORE_PEER_MSPCONFIGPATH=${CEPA21_MSPCONFIGPATH}
CORE_PEER_TLS_ROOTCERT_FILE=${CEPA21_TLS_ROOTCERT_FILE}
peer chaincode install -n baw-contract -v 1.0 -p "$CC_SRC_PATH" -l "$CC_RUNTIME_LANGUAGE"

# Installing smart contract on peer1.cepa21.baw-network.com
CORE_PEER_LOCALMSPID=Cepa21MSP
CORE_PEER_ADDRESS=peer1.cepa21.baw-network.com:8051
CORE_PEER_MSPCONFIGPATH=${CEPA21_MSPCONFIGPATH}
CORE_PEER_TLS_ROOTCERT_FILE=${CEPA21_TLS_ROOTCERT_FILE}
peer chaincode install -n baw-contract -v 1.0 -p "$CC_SRC_PATH" -l "$CC_RUNTIME_LANGUAGE"

# Installing smart contract on peer0.crdo.baw-network.com
CORE_PEER_LOCALMSPID=CrdoMSP
CORE_PEER_ADDRESS=peer0.crdo.baw-network.com:9051
CORE_PEER_MSPCONFIGPATH=${CRDO_MSPCONFIGPATH}
CORE_PEER_TLS_ROOTCERT_FILE=${CRDO_TLS_ROOTCERT_FILE}
peer chaincode install -n baw-contract -v 1.0 -p "$CC_SRC_PATH" -l "$CC_RUNTIME_LANGUAGE"

# Installing smart contract on peer1.crdo.baw-network.com
CORE_PEER_LOCALMSPID=CrdoMSP
CORE_PEER_ADDRESS=peer1.crdo.baw-network.com:10051
CORE_PEER_MSPCONFIGPATH=${CRDO_MSPCONFIGPATH}
CORE_PEER_TLS_ROOTCERT_FILE=${CRDO_TLS_ROOTCERT_FILE}
peer chaincode install -n baw-contract -v 1.0 -p "$CC_SRC_PATH" -l "$CC_RUNTIME_LANGUAGE"

# INSTANTIATING SMART CONTRACT ON CHANNELS
# Instantiating smart contract on cepa21crdo
CORE_PEER_LOCALMSPID=Cepa21MSP
CORE_PEER_MSPCONFIGPATH=${CEPA21_MSPCONFIGPATH}
peer chaincode instantiate \
    -o orderer.baw-network.com:7050 \
    -C cepa21crdo \
    -n baw-contract \
    -l "$CC_RUNTIME_LANGUAGE" \
    -v 1.0 \
    -c '{"Args":["a","20"]}' \
    -P "AND('Cepa21MSP.member','CrdoMSP.member')" \
    --tls \
    --cafile ${ORDERER_TLS_ROOTCERT_FILE} \
    --peerAddresses peer0.cepa21.baw-network.com:7051 \
    --tlsRootCertFiles ${CEPA21_TLS_ROOTCERT_FILE}
