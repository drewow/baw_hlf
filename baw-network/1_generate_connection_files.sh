#!/bin/bash

# ---------------------------------------------------------------------------
# Clear screen
# ---------------------------------------------------------------------------
clear

echo "# ---------------------------------------------------------------------------"
echo "# Create channel-artifacts folder"
echo "# ---------------------------------------------------------------------------"

if [ ! -d channel-artifacts ]; then
    mkdir channel-artifacts
fi

echo "# ---------------------------------------------------------------------------"
echo "# Remove old crypto material"
echo "# ---------------------------------------------------------------------------"

if [ -d crypto-config ]; then
    rm -R crypto-config
fi

#mkdir channel-artifacts
#rm -R crypto-config

echo "# ---------------------------------------------------------------------------"
echo "# Generate crypto material"
echo "# ---------------------------------------------------------------------------"

../bin/cryptogen generate --config crypto-config.yaml

ORG_B=Cepa21
ORG=cepa21
P0PORT=7051
P1PORT=8051
CAPORT=7054
PEERPEM=crypto-config/peerOrganizations/cepa21.baw-network.com/tlsca/tlsca.cepa21.baw-network.com-cert.pem
CAPEM=crypto-config/peerOrganizations/cepa21.baw-network.com/ca/ca.cepa21.baw-network.com-cert.pem

PP="`awk 'NF {sub(/\\n/, ""); printf "%s\\\\\\\n",$0;}' $PEERPEM`"
CP="`awk 'NF {sub(/\\n/, ""); printf "%s\\\\\\\n",$0;}' $CAPEM`"

echo "$(
sed -e "s/\${ORG}/$ORG/" \
-e "s/\${ORG_B}/$ORG_B/" \
-e "s/\${P0PORT}/$P0PORT/"  \
-e "s/\${P1PORT}/$P1PORT/" \
-e "s/\${CAPORT}/$CAPORT/"  \
-e "s#\${PEERPEM}#$PP#"  \
-e "s#\${CAPEM}#$CP#" \
./ccp-template.json \
)" > connection-cepa21.json

echo "$(
sed -e "s/\${ORG}/$ORG/" \
-e "s/\${ORG_B}/$ORG_B/" \
-e "s/\${P0PORT}/$P0PORT/"  \
-e "s/\${P1PORT}/$P1PORT/" \
-e "s/\${CAPORT}/$CAPORT/"  \
-e "s#\${PEERPEM}#$PP#"  \
-e "s#\${CAPEM}#$CP#" \
./ccp-template.yaml \
)" > connection-cepa21.yaml



ORG_B=Crdo
ORG=crdo
P0PORT=9051
P1PORT=10051
CAPORT=8054
PEERPEM=crypto-config/peerOrganizations/crdo.baw-network.com/tlsca/tlsca.crdo.baw-network.com-cert.pem
CAPEM=crypto-config/peerOrganizations/crdo.baw-network.com/ca/ca.crdo.baw-network.com-cert.pem

PP="`awk 'NF {sub(/\\n/, ""); printf "%s\\\\\\\n",$0;}' $PEERPEM`"
CP="`awk 'NF {sub(/\\n/, ""); printf "%s\\\\\\\n",$0;}' $CAPEM`"

echo "$(
sed -e "s/\${ORG}/$ORG/" \
-e "s/\${ORG_B}/$ORG_B/" \
-e "s/\${P0PORT}/$P0PORT/"  \
-e "s/\${P1PORT}/$P1PORT/" \
-e "s/\${CAPORT}/$CAPORT/"  \
-e "s#\${PEERPEM}#$PP#"  \
-e "s#\${CAPEM}#$CP#" \
./ccp-template.json \
)" > connection-crdo.json

echo "$(
sed -e "s/\${ORG}/$ORG/" \
-e "s/\${ORG_B}/$ORG_B/" \
-e "s/\${P0PORT}/$P0PORT/"  \
-e "s/\${P1PORT}/$P1PORT/" \
-e "s/\${CAPORT}/$CAPORT/"  \
-e "s#\${PEERPEM}#$PP#"  \
-e "s#\${CAPEM}#$CP#" \
./ccp-template.yaml \
)" > connection-crdo.yaml