package main

import (
	"encoding/json"
	"fmt"

	"github.com/hyperledger/fabric-contract-api-go/contractapi"
)

// WineContract contract for storing data in blockchain
type WineContract struct {
	contractapi.Contract
}

// UpdateUserSettings updates the parameters in user settings
func (w *WineContract) UpdateUserSettings(ctx contractapi.TransactionContextInterface, userID string, name string, cards []int32) (UserSetting, error) {

	userSetting := UserSetting{
		UserID: userID,
		Name:   name,
		Cards:  cards,
	}

	newUserSettingAsBytes, _ := json.Marshal(userSetting)
	return userSetting, ctx.GetStub().PutState(userID, newUserSettingAsBytes)
}

// GetUserSettings gets the settings of user
func (w *WineContract) GetUserSettings(ctx contractapi.TransactionContextInterface, userID string) (*UserSetting, error) {
	userSettingAsBytes, err := ctx.GetStub().GetState(userID)

	if err != nil {
		return nil, fmt.Errorf("Failed to read from Blockchain. %s", err.Error())
	}
	if userSettingAsBytes == nil {
		return nil, fmt.Errorf("User with userid %s not exist on Blockchain", userID)
	}

	var userSetting UserSetting
	json.Unmarshal(userSettingAsBytes, &userSetting)
	return &userSetting, nil
}

// StartNewRoute start the new route
func (w *WineContract) StartNewRoute(ctx contractapi.TransactionContextInterface, entryID string, routeID string, userID string, lpn string, time string, gps string, lpnImage string, lpnImageHash string) (map[string]string, error) {
	coordinates := Coordinates{
		Code:         1,
		RouteID:      routeID,
		UserID:       userID,
		LPN:          lpn,
		Time:         time,
		GPS:          gps,
		LPNPhoto:     lpnImage,
		LPNPhotoHash: lpnImageHash,
	}

	routeAsBytes, _ := json.Marshal(coordinates)
	err := ctx.GetStub().PutState(entryID, routeAsBytes)
	if err != nil {
		return nil, fmt.Errorf("Error while updating coordinates in Blockchain %s", err.Error())
	}

	response := map[string]string{
		"code":   "1",
		"route":  routeID,
		"status": "transit",
	}
	return response, nil
}

// UpdateCurrentCoordinate updates the coordinate of vehicle
func (w *WineContract) UpdateCurrentCoordinate(ctx contractapi.TransactionContextInterface, entryID string, userID string, routeID string, time string, gps string) (map[string]string, error) {
	coordinates := Coordinates{
		Code:    2,
		EntryID: entryID,
		UserID:  userID,
		RouteID: routeID,
		Time:    time,
		GPS:     gps,
	}

	coordinateAsBytes, _ := json.Marshal(coordinates)
	err := ctx.GetStub().PutState(entryID, coordinateAsBytes)
	if err != nil {
		return nil, fmt.Errorf("Error while updating coordinates in Blockchain %s", err.Error())
	}

	response := map[string]string{
		"code":     "2",
		"entry_id": entryID,
		"route":    routeID,
		"status":   "transit",
	}
	return response, nil
}

// FinishRoute finishes route and updates last coordinates
func (w *WineContract) FinishRoute(ctx contractapi.TransactionContextInterface, entryID string, userID string, routeID string, time string, gps string, weight int64, weightImg string, weightImgHash string, netto int64, nettoImg string, nettoImgHash string, degrees float64, degreesImg string, degreesImgHash string, mixtures []Mixture) (map[string]string, error) {
	coordinates := Coordinates{
		Code:           3,
		RouteID:        routeID,
		Time:           time,
		GPS:            gps,
		Weight:         weight,
		WeightImg:      weightImg,
		WeightImgHash:  weightImgHash,
		Netto:          netto,
		NettoImg:       nettoImg,
		NettoImgHash:   nettoImgHash,
		Degrees:        degrees,
		DegreesImg:     degreesImg,
		DegreesImgHash: degreesImgHash,
		Mixtures:       mixtures,
	}

	coordinateAsBytes, _ := json.Marshal(coordinates)
	err := ctx.GetStub().PutState(entryID, coordinateAsBytes)
	if err != nil {
		return nil, fmt.Errorf("Error while updating coordinates in Blockchain %s", err.Error())
	}

	response := map[string]string{
		"code":     "3",
		"doc":      "",
		"entry_id": entryID,
		"route":    routeID,
		"status":   "finished",
	}
	return response, nil
}

// GetRoutesByTransporter gets routes recorded by perticular user
func (w *WineContract) GetRoutesByTransporter(ctx contractapi.TransactionContextInterface, userID string) ([]Coordinates, error) {
	queryString := fmt.Sprintf(`{
		"selector": {
		   "route_id": {
			  "$gt": null
		   },
		   "user_id": "%s"
		}
	}`, userID)

	resultIterator, err := ctx.GetStub().GetQueryResult(queryString)
	if err != nil {
		return nil, fmt.Errorf("Error while quering data from Blockchain")
	}
	defer resultIterator.Close()

	entries := []Coordinates{}
	for resultIterator.HasNext() {
		entry, _ := resultIterator.Next()
		c := new(Coordinates)
		_ = json.Unmarshal(entry.Value, c)
		if len(c.Mixtures) == 0 {
			c.Mixtures = []Mixture{}
		}
		entries = append(entries, *c)
	}
	return entries, nil
}

// GetAllRoutes gets all recorded routes
func (w *WineContract) GetAllRoutes(ctx contractapi.TransactionContextInterface) ([]Coordinates, error) {
	queryString := fmt.Sprintf(`{
		"selector": {
		   "route_id": {
			  "$gt": null
		   }
		}
	}`)

	resultIterator, err := ctx.GetStub().GetQueryResult(queryString)
	if err != nil {
		return nil, fmt.Errorf("Error while quering data from Blockchain")
	}
	defer resultIterator.Close()

	entries := []Coordinates{}
	for resultIterator.HasNext() {
		entry, _ := resultIterator.Next()
		c := new(Coordinates)
		_ = json.Unmarshal(entry.Value, c)
		if len(c.Mixtures) == 0 {
			c.Mixtures = []Mixture{}
		}
		entries = append(entries, *c)
	}
	return entries, nil
}
