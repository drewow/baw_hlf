package main

// UserSetting settings of different users
type UserSetting struct {
	UserID string  `json:"user_id"`
	Name   string  `json:"name"`
	Cards  []int32 `json:"cards"`
}

// Mixture card and weight
type Mixture struct {
	Card   int64   `json:"card"`
	Weight float64 `json:"weight"`
}

// Coordinates coordinates of vehicle at every 15 seconds
type Coordinates struct {
	UserID         string    `json:"user_id"`
	RouteID        string    `json:"route_id"`
	Code           int16     `json:"code"`
	Degrees        float64   `json:"degrees"`
	DegreesImg     string    `json:"degrees_img"`
	DegreesImgHash string    `json:"degrees_img_hash"`
	EntryID        string    `json:"entry_id"`
	GPS            string    `json:"gps"`
	LPN            string    `json:"lpn"`
	LPNPhoto       string    `json:"lpn_photo"`
	LPNPhotoHash   string    `json:"lpn_photo_hash"`
	Mixtures       []Mixture `json:"mixtures"`
	Netto          int64     `json:"netto"`
	NettoImg       string    `json:"netto_img"`
	NettoImgHash   string    `json:"netto_img_hash"`
	Time           string    `json:"time"`
	Weight         int64     `json:"weight"`
	WeightImg      string    `json:"weight_img"`
	WeightImgHash  string    `json:"weight_img_hash"`
}
