module github.com/chaincodes/block-n-wine

go 1.13

require (
	github.com/DATA-DOG/godog v0.7.13 // indirect
	github.com/gogo/protobuf v1.2.1 // indirect
	github.com/hyperledger/fabric-chaincode-go v0.0.0-20200424173110-d7076418f212
	github.com/hyperledger/fabric-contract-api-go v1.1.0
)