package main

import (
	"fmt"
	"github.com/hyperledger/fabric-chaincode-go/shim"
	"baw-contract/contract"
)

// main function starts up the chaincode in the container during instantiate
func main() {
	if err := shim.Start(new(contract.BawContract)); err != nil {
		fmt.Printf("Error starting Block&WinContract chaincode: %s", err)
	}
}
