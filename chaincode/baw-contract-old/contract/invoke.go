package contract

import (
	"github.com/hyperledger/fabric-chaincode-go/shim"
	"github.com/hyperledger/fabric-protos-go/peer"
	"baw-contract/contract/action/route"
	"baw-contract/contract/action/user"
)

// Invoke is called per transaction on the chaincode. Each transaction is
// either a 'get' or a 'set' on the asset created by Init function. The Set
// method may create a new asset by specifying a new key-value pair.
func (t *BawContract) Invoke(stub shim.ChaincodeStubInterface) peer.Response {
	// Extract the function and args from the transaction proposal
	fn, args := stub.GetFunctionAndParameters()

	var result string
	var err error

	switch fn {
	case "startRoute":
		result, err = route.startRoute(stub, args)
	case "updateCoords":
		result, err = route.updateCoords(stub, args)
	case "finishRoute":
		result, err = route.finishRoute(stub, args)
	case "getRoutes":
		result, err = route.getRoutes(stub, args)
    case "updatePersonalData":
		result, err = user.setPersonalData(stub, args)
    case "insertUser":
		result, err = user.createUser(stub, args)
	default:
		result, err = route.GetRoutes(stub, args)
	}

	if err != nil {
		return shim.Error(err.Error())
	}

	// Return the result as success payload
	return shim.Success([]byte(result))
}
