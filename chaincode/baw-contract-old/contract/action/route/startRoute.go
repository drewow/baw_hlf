package agreement

import (
	"encoding/json"
	"errors"
	"fmt"
	"github.com/hyperledger/fabric-chaincode-go/shim"
	"baw-contract/contract/model"
	"baw-contract/contract/service"
	"strconv"
)

func startRoute(stub shim.ChaincodeStubInterface, args []string) (string, error) {
	if len(args) != 3 {
		return "", fmt.Errorf("Incorrect arguments. Expecting lpn, time, gps, and lpn_image")
	}

	user, err := service.NewAuthService(stub).GetUser()
	if err != nil {
		return "", err
	}

	lpn, time, gps, lpn_image := args[0], args[1], args[2], args[3]
	timestamp, err := strconv.ParseInt(time, 10, 64)
	if err != nil {
		return "", err
	}






	var agreement model.Agreement
	agreementService := service.NewAgreementService(stub)
	bytes := agreementService.Find(key)

	if bytes != nil {
		return "", fmt.Errorf("Agreement with the same key is already created", key)
	}

	agreement = agreementService.Create(key, doctor, user.Id, timestamp)
	jsonBytes, err := json.Marshal(agreement)
	if err != nil {
		return  "", fmt.Errorf("Failed to marshall agreement obj", key)
	}

	err = agreementService.Put(key, jsonBytes)
	if err != nil {
		return  "", err
	}

	return string(jsonBytes), nil
}