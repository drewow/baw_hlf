package model

const STARTED_STATUS = "STARTED"
const TRANSIT_STATUS = "TRANSIT"
const FINISHED_STATUS = "FINISHED"

type Route struct {
	Id          string `json:"id"`
	IdUser      string `json:"idUser"`
	Date        int64  `json:"date"`
	Lpn         string  `json:"lpn"`
	Lpn_img     string  `json:"lpn_img"`
	Weight      int64  `json:"weight"`
	Weight_img  int64  `json:"weight_img"`
	Netto       int64  `json:"netto"`
	Netto_img   int64  `json:"netto_img"`
	Degrees     int64  `json:"degrees"`
	Degrees_img int64  `json:"degrees_img"`
    Traces []TraceItem `json:"traces"`
}

type TraceItem struct {
	Id          string `json:"id"`
	IdRoute     string `json:"idRoute"`
	time        int64  `json:"time"`
	gps         string  `json:"gps"`
	code        string  `json:"code"`
}