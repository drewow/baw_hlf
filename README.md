# Network

## Goal

The main goal of the Network is to provide private transactions between network participants. The network is built using the Hyperledger Fabric 1.4 framework.

## Network members

Hyperledger Fabric corporate network members are Cepa21, and CRDO. Members of these organizations interact with each other in one single channel. The first bawfirst channel is for conducting private transactions between Cepa21 and CRDO organizations.

In the bawfirst channel, data collected by wine producers from Cepa21 organization is stored via chaincode in this enterpise blockchain, allowing access to it for both parties, CRDO and Cepa21.

## Network deployment

The network is installed on 3 servers using Docker Swarm. All servers must have git and docker installed. As a result of further actions, each network component will be installed on each server:

1 server - Orderer, MySQL DB (for storing some information)
2 server - Cepa21 organization (Peer0, Peer1), CouchDB, Fabric-CA, CLI
3 server - CRDO organization (Peer0, Peer1), CouchDB, Fabric-CA

Here are the commands for all three servers, including shell scripts for proper configuration, as well as chaincodes:
```
git clone https://drewow@bitbucket.org/drewow/baw_hlf.git network
```
On the first server, we will create all the necessary certificates necessary for the operation of our network:
```
cd network/baw-network
./1_generate_connection_files.sh
./2_generating_channel_configuration.sh
```
It is also necessary to copy the resulting channel-artifacts and crypto-config folders to the other two servers.

Creating a Docker Swarm stack begins by initializing it on the first server and creating a join-token for the rest of the servers:
```
docker swarm init
docker swarm join-token manager
```
Now we can attach the rest of the servers to the stack (run on servers 2, and 3):
```
docker swarm join --token SWMTKN-1-2xzco7t7txohnzd09318eczpbgmm8woex80byxptpt1jl5i2ar-bsg37h40xze1gaabg80i96gw2 172.31.38.245:2377
```
Here it will be necessary to replace the token with the one that appears as a result of the "docker swarm join-token manager" command.

On the first server in the network / baw-network folder there is a file .env.template, in which you need to register hostname for each of the hosts:

ORDERER = ip-172-31-38-245
CEPA21 = ip-172-31-43-64
CRDO = ip-172-31-38-130

Next, we need to generate the .env file
```
./3_env_gen.sh
```
## Creating a Swarm Stack
```
env $ (cat .env | grep ^ [A-Z] | xargs) docker stack deploy -c docker-compose-general.yaml stage 2> & 1
```
After the stack is created, you need to go to the docker CLI container on the second server:
```
docker exec -ti stage_cli.1.owni217t53m53efjtikb5oa2f/bin/bash
```
And inside this container, execute all the commands that are contained in the 4_create_channels.sh file at network / poc-network. These commands will create channels, attach all peers to the channel, and establish smart contracts.

## License
The project is developed by [NIX Solutions](http://nixsolutions.com) Go team and distributed under [MIT LICENSE](https://github.com/nixsolutions/blockchain-poc-network/blob/master/LICENSE)